const fs = require("fs");
const path = require("path");

module.exports = ctx => ({
  title: 'Vistacollege Handboek',
  description: 'Het handbook voor de lessen van school.',
  base: '/vistadocs/',
  dest: 'public',
  themeConfig: {
    repo: 'https://gitlab.com/vistacollege',
    editLinks: true,
    docsDir: 'docs/',
    logo: '/GeoLogo.png',
    smoothScroll: true,
    sidebarDepth: 3,
    yuu: {
      defaultDarkTheme: true,
    },

    nav: [
      {
        text: 'Home',
        link: '/'
      },
    ],

    sidebar: [{
      collapsable: true,
      title: '🔒 Security/Beveiliging',
      children: ['/vistacollege/security/case-1/laptop-directeur', '/vistacollege/security/case-2/helpdesk/kiosk/kiosk'],
    },
    {
      collapsable: true,
      title: '🐧 Linux',
      children: ['/vistacollege/linux/opdrachten/mint-installatie',
      '/vistacollege/linux/opdrachten/instellingen-en-programmas',
      '/vistacollege/linux/opdrachten/opdr5',
      '/vistacollege/linux/opdrachten/opdr6',
      '/vistacollege/linux/opdrachten/opdr7',
      '/vistacollege/linux/opdrachten/opdr8'],
    },
    ],
    
  },
  head: [
    ['link', {
      rel: 'icon',
      href: '/PBST-Logo.png'
    }],
    ['link', {
      rel: 'manifest',
      href: '/manifest.json'
    }],
    ['meta', {
      name: 'theme-color',
      content: '#3eaf7c'
    }]
  ],
  plugins: [
    [
      'vuepress-plugin-zooming',
      {
        selector: '.theme-default-content img.zooming',
        delay: 1000,
        options: {
          bgColor: 'white',
          zIndex: 10000,
        },
      },
    ],
    ['@vuepress/pwa',
      {
        serviceWorker: true,
        updatePopup: true
      }
    ],
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-168777162-2' // UA-00000000-0
      }
    ],
    ['vuepress-plugin-global-toc'],
    [
      'vuepress-plugin-copyright',
      {
        noCopy: true, // the selected text will be uncopiable
        minLength: 100, // if its length is greater than 100
      },
    ],
  ],
})


function getSideBar(folder, title) {
  const extension = [".md"];

  const files = fs
    .readdirSync(path.join(`${__dirname}/../${folder}`))
    .filter(
      (item) =>
        item.toLowerCase() != "readme.md" &&
        fs.statSync(path.join(`${__dirname}/../${folder}`, item)).isFile() &&
        extension.includes(path.extname(item))
    );

  return [{ title: title, children: ["", ...files] }];
}