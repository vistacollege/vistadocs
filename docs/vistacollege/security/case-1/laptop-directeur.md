# 💻 Laptop Directeur
In deze opdracht moeten wij een werkstation verbinden met de server. Hiervoor heb ik VMWare gebruikt, en dit zal ik ook doen met VirtualBox.

## 👩‍🏫 Handleiding werkend krijgen van opdracht
Hierin wordt uitgelegd hoe je virtualbox en de mailserver, router en laptop moet instellen in **VIRTUALBOX**
:::warning Dit is bedoeld voor SPL & virtualbox gebruikers!  
Deze handleiding is gemaakt voor de Stichting Praktijk Leren opdracht van "Nieuwe Laptop" uit Case 1 bij Security 3. 
:::


## 📦 Installeer Virtualbox

1. Ga naar [deze website](https://www.virtualbox.org/wiki/Downloads)  
2. Klik op deze knop voor een windows installatie.  
![picture 2](/vistadocs/images/4ed474a0828ca6a89e4f5c15dcfb014b79909032c657eca4a9373de0db9927f1.png)  
3. Daarnaa installeer je virtualbox zoals de leraar dit wilt hebben.  
![picture 3](/vistadocs/images/fd7b9662be92b773c3ebf95d0413c7a7beac9caa4a92f5d4258eaa044c28f27e.png)  
![picture 4](/vistadocs/images/50f92809637762ef205ea31b4f7d709c47b8b44120d8b45d70f42d3d8cbe52b9.png)  
![picture 5](/vistadocs/images/f6bfd56ba66592feb3b24b18ad4f0a73b0a4714e1d938357495a2f98aeeeed65.png)  
4. Zorg ervoor dat je altijd leest wat een installatie zegt!!!  
![picture 6](/vistadocs/images/97dbc64a11427b11b687e6efa2cb424e57adfe33ca91a51065b3240873bc0946.png)  
![picture 7](/vistadocs/images/c602af80b104237b13bdc2c4276621d2d3ae2385ec9e8f14aff921c0825b6eaf.png)

### 👷‍♂️ Instellingen Virtualbox
5. Ga naar de virtualbox instellingen ***Host Network Manager***  
![picture 8](/vistadocs/images/a7669f392b562153864fd985565bfdb78b536ca78f73ee69d7058357470d2743.png)  

6. Open de instellingen van de internet adapter in die instellingen  
7. Zet de DHCP server uit.  
8. Verander het IP adres naar "``192.168.0.1``".
![picture 9](/vistadocs/images/92020e3b31439c37f19b91e436e62e990b3716fe5b6696eb366fc8ff7060994b.png)  

9. Op SFL ga je naar de ``Security Case Niveau 3 V2`` pagina "``Download VM's``". En hier download je:
    - [SECN3V2-Server.7z](https://www.stichtingpraktijkleren.nl/fileadmin/data/60-ICT/multimediale%20praktijkcases/virtualbox/SECN3/SECN3V2-Server.7z)
    - [SECN3V2-Laptop-Falcon.7z](https://www.stichtingpraktijkleren.nl/fileadmin/data/60-ICT/multimediale%20praktijkcases/virtualbox/SECN3/SECN3V2-Laptop-Falcon.7z)
    - [SECN3V2-Router.7z](https://www.stichtingpraktijkleren.nl/fileadmin/data/60-ICT/multimediale%20praktijkcases/virtualbox/SECN3/SECN3V2-Router.7z) (Deze is **ALEEN** nodig voor VirtualBox nodig)


### Installatie Thunderbird
1. Start de virtuele machine op, en download het volgende programma's:
    - [🐦 Thunderbird](https://www.thunderbird.net/nl/)

### Configuratie Thunderbird'
1. Begin met het configureren van thunderbird.

:::warning Waarom niet gewoon windows mail?
Windows mail kan de veilige verbinding niet (meer) maken, hierdoor kan je niet met de email server verbinden omdat het geen "veilige connectie" is.
:::

## 📬 Spamfilter
### 👮‍♂️ Werking spamfilter
Thunderbird heeft een "Zelflerend Spamfilter", hierbij moet je het programma aangeven wat een spam email is, hieruit zal Thunderbird leren "Dit is een spam email, haal het weg".  

### 🔥 Markeer een email als spam
Als je emails ontvangt, kom je bij dit scherm:  
![emails-geen-filtter](https://i.imgur.com/vyZSMVj.png)

Hieruit ga je dan kijken naar de emails die gefiltered kunnen worden als spam. Heb je een email die je wilt classificeren als spam? Druk dan op de vlam recht in de email balk:  
![filter-enabled-filter](https://i.imgur.com/6Ag4n8E.png)  

Zodra je hier op drukt, zullen andere emails die in de buurt komen, vanzelf verplaasts worden naar de "Ongewensten" map.  
![Gevulde-filter-map](https://i.imgur.com/3zUmBp6.png)  

## 💾 Backups
Zodra je een backup wilt hebben van je bestanden, ga je naar de **File History** instellingen. Deze vind je door "``backup``" te typen in de zoekbalk van windows.

### 📂 Voeg een folder toe
Als je bij dit menu aankomt, klik je op "**More options**"  
![Backup-file-history](https://i.imgur.com/7jahQlu.png)

Dan kom je op dit scherm: 
![backup-screen](https://i.imgur.com/QszaKKo.png)

Zodra dat is gelukt, kan je drukken op "**Add a folder**", die opent de explorer en daar kan je de folder selecteren die je wilt backuppen:  
![backup-add-a-folder](https://i.imgur.com/tD3t4e7.png)

### ⏪ Hoe zet je bestanden terug
Zodra er iets mis gaat kan je terug gaan naar de folder waar de backup is opgeslagen. In dit geval is dat nu de harde schrijf waar de laptop op staat ingesteld.
![zeg-bestanden-terug](https://i.imgur.com/9umVzc1.png)

Dit is een simpel geval van drag en drop 😊