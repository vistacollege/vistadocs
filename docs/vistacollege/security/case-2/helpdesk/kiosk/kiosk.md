# Kiosken

## Installatie van de kiosk
*Standaard windows installatie*

## Configureren
1. Ga naar [deze website (Google Chrome)](https://www.google.com/intl/nl_nl/chrome/) om chrome te downloaden op de machine. En open dit bestand dan.  
2. Ga naar [deze website (AnyDesk)](https://anydesk.com/) om anydesk te downloaden.  
3. Open AnyDesk, en open de instellingen. Verander de veiligheidsinstellingen om een wachtwoord toe te voegen.  
![picture 1](/vistadocs/images/73e786c0ae85fde1824fda98ea9c242c084cf5a8ca353e3faf929f97b900606b.png)  

