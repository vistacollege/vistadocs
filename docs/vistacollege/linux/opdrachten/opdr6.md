# Opdracht 6

## Backup Herstellen

Om een backup te herstellen doet u het volgende:
![img](/vistadocs/opdr6/media/image1.png)  
Klik links onder op het Linux icoon en typ in de zoekback "Reservekopie-gereedschap" in. Klik op de optie "Reservekopie-gereedschap" in dit veld kunt u uw backup herstellen

## Timeshift
Om gebruik te maken van Timeshirt en een snapshot te maken doet u het volgende:
![img](/vistadocs/opdr6/media/image2.png)  
Klik linksonder op het Linux icoon en typ "Timeshift" in. Klik op "Timeshift" in dit veld kunt u op "create" klikken, hiermee maakt u een snapchat van uw systeem

## Linux ouderlijk toezicht

### Timekpr
Om Timekpr te gebruiken doet u het volgende:
![img](/vistadocs/opdr6/media/image3.png)  
Installeer de ppa "mjasnik"
![img](/vistadocs/opdr6/media/image4.png)  
Ga vervolgens naar programmabeheer, klik rechtsboven op het menu icoon en kies "ververs de pakkettenlijst"

## Linux printen
![img](/vistadocs/opdr6/media/image5.png)  
Om zelf uw driver te kiezen doe je het volgende:
![img](/vistadocs/opdr6/media/image6.png)  
Klik linksonder op het Linux icoon ga naar beheer en kies "afdrukbeheer". Klik op "afdrukbeheer"
![img](/vistadocs/opdr6/media/image7.png)  
U komt dat bij dit scherm terecht, klik op "toevoegen"
![img](/vistadocs/opdr6/media/image8.png)  
## Linux Uiterlijk
### Instellingen en Tweak Tools**
Als u gebruik wilt maken van instelling en Tweak Tools doet u het volgende:
![img](/vistadocs/opdr6/media/image9.png)  
Klik linksonder op het Linux icoon, selecteer "voorkeuren" en klik op "Thema's"
![img](/vistadocs/opdr6/media/image10.png)  
Hier kunt u uw thema's naar keuze veranderen en instellen naar uw voorkeur
### Persoonlijk menu
Wil u gebruik maken van een persoonlijk menu dan doet u het volgende:
![img](/vistadocs/opdr6/media/image11.png)  
Klik met de rechter muisknop linksonder op het Linux icoon en klik op "Instellen"
![img](/vistadocs/opdr6/media/image12.png)  
U komt dan in dit veld terecht, klik op "menu" bovenaan het scherm. Als u op "De menubewerker openen" klikt, kunt u het menu zelf bewerken.
![img](/vistadocs/opdr6/media/image13.png)  
Hier kunt u bijvoorbeeld bepaalde programma's uit het startmenu halen or juist toevoegen.
