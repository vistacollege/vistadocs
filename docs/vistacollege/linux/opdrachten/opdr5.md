# Appplets - (Opdracht 5)
## Om automatisch rangschikken uitzetten
![img](/vistadocs/opdr5/media/image1.png)  
Klik met de rechter muisknop op het bureaublad en vink 'uitgelijnd houden' uit

## Applet toevoegen: 
![img](/vistadocs/opdr5/media/image2.png)  
Klik me de rechtermuisknop op het bureaublad en kies "add desklets"


![img](/vistadocs/opdr5/media/image3.png)  
![img](/vistadocs/opdr5/media/image4.png)  
Je komt dan in dit scherm terecht, selecteer hier de applet die je wilt en klik links onder op de plus.
## Hulpjes toevoegen

![img](/vistadocs/opdr5/media/image5.png)  
Klik met de rechter muisknop op de werkbalk en kies "Add applets tot he panel"

![img](/vistadocs/opdr5/media/image6.png)  
Dat kom je in dit scherm, hier kan je selecteren wat je wilt.

![img](/vistadocs/opdr5/media/image7.png)  

Als je actieve hoeken wilt instellen doe je het Volgende:![img](/vistadocs/opdr5/media/image8.png)  Klik rechtsonder op het Linux logo en typ "actieve hoeken" in en klik er op
![img](/vistadocs/opdr5/media/image9.png)  
Hier kan je de gewenste hoeken aan en uit zetten.


Om snelkoppelingen toe te voegen doe je het volgende:

![img](/vistadocs/opdr5/media/image10.png)  
![img](/vistadocs/opdr5/media/image11.png)  
![img](/vistadocs/opdr5/media/image12.png)  
Klik linksonder op het Linux logo en zoek het gewenste programma, klik met de rechter muisknop op het programma en klik op "Toevoegen op bureaublad"

Om een sneltoetscombinatie te maken doe je het volgende:

![img](/vistadocs/opdr5/media/image13.png)  
klik linksonder op het Linux logo en typ "toetsenbord" Klik daar op, ga dan naar sneltoetsen, klik op "voeg aangepaste sneltoets toe" Hier kan je een sneltoets toevoegen

Als je de chromium browser wilt downloaden en instellen als je voorkeurstoepassing en ook in je werkbalk zetten doe je het volgende:

![img](/vistadocs/opdr5/media/image14.png)  
Klik linksonder op het linux logo en typ "programmabeheer" in, klik daar op





![img](/vistadocs/opdr5/media/image15.png)  
Klik rechtsboven op de zoekbalk en typ "chromium" in. Nu word er gezocht naar de chromium browser.
![img](/vistadocs/opdr5/media/image16.png)  
Klik vervolgens op de chromium browser
