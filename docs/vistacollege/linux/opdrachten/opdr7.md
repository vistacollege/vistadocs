# Opdracht 7

## De correcte drivers

![img](/vistadocs/opdr7/media/image1.png)
Ik heb in mijn terminal ``sudo apt-get purge nvidia`` getypt

![img](/vistadocs/opdr7/media/image2.png)
Hier probeerde ik ``sudo add-apt-repository ppa:graphics-driversppa`` in te typen maar kreeg ik deze error

![img](/vistadocs/opdr7/media/image3.png)
Hier probeerde ik ``sudo apt-get update`` in te typen maar kreeg ik deze error

![img](/vistadocs/opdr7/media/image4.png)
Hier probeerde ik ``Sudo add-apt-repository ppa:oibafgraphics-drivers`` in te typen maar kreeg ik deze error

![img](/vistadocs/opdr7/media/image5.png)
Hier probeerde ik ``sudo apt-get update`` in te typen maar kreeg ik deze error

## Native Linux games

![img](/vistadocs/opdr7/media/image6.png)
Voor de rest kon ik geen internet verbinding krijgen. (Virtualbox deed irritant)