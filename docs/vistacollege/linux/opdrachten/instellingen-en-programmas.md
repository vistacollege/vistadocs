# Instellingen en programma's (Opdr 4)  
 
## Inloggen Stuurprogramma beheer
![image](/vistadocs/images/Opdracht-4-Stefano-%20(1).png)  
Hier voer ik mijn wachtwoord in, om in het stuurprogramma beheer te komen

## Cache Updaten
![image](/vistadocs/images/Opdracht-4-Stefano-%20(2).png)  
Dit is het updaten van de cache

## Stuur Programma's
![image](/vistadocs/images/Opdracht-4-Stefano-%20(3).png)
Er zijn geen alternatieve stuurprogramma beschikbaar 

## Welkomsscherm
![image](/vistadocs/images/Opdracht-4-Stefano-%20(4).png)  
Dit is het welkom scherm van Linux. Er zijn hier een aantal optie te vinden, bijvoorbeeld:

### 
![image](/vistadocs/images/Opdracht-4-Stefano-%20(5).png)  
In de tab “Eerste schreden” kan je dingen vinden zoals: Momentopnamen van het systeem, stuurprogrammabeheer, bijwerkbeheer, systeeminstellingen, programmabeheer en de firewall.

![image](/vistadocs/images/Opdracht-4-Stefano-%20(6).png)  
Dit is de momentopname optie

![image](/vistadocs/images/Opdracht-4-Stefano-%20(7).png)  
Dit is de stuurprogrammabeheer optie

![image](/vistadocs/images/Opdracht-4-Stefano-%20(8).png)  
Dit is de bijwerkbeheer optie

![image](/vistadocs/images/Opdracht-4-Stefano-%20(9).png)  
Dit is de systeeminstellingen optie

![image](/vistadocs/images/Opdracht-4-Stefano-%20(10).png)  
Dit is de programmabeheer optie

![image](/vistadocs/images/Opdracht-4-Stefano-%20(11).png)  
Dit is de firewall optie

![image](/vistadocs/images/Opdracht-4-Stefano-%20(12).png)  
Klik links onder op het Linux logo, en typ “schermbeveiliging” in. Klik op schermbeveiliging 

![image](/vistadocs/images/Opdracht-4-Stefano-%20(13).png)  
In de schermbeveiliging optie kan je een aantal dingen veranderen, bijvoorbeeld de schermbeveiliging en de verschillende thema’s 

![image](/vistadocs/images/Opdracht-4-Stefano-%20(14).png)  
In de kale installatie van Linux zitten een aantal standaard apps. Deze zijn: Google earth, dropbox, cheese, minecraft, blender, gnome maps, sublime, vlc, steam, gparted, whatsapp en spotify.

**Google Earth**: Is een gratis applicatie van Google waarmee men vrijwel elke plek op de wereld kan opzoeken met behulp van satelliet en luchtfoto's (Zit niet standaard in Windows)  

**Dropbox:** Is een clouddienst voor het online opslaan van bestanden. (Zit standaard in Windows)  

**Cheese**: Is een gnome webcam applicatie. (Zit niet standaard in Windows)  

**Minecraft**: Is een *sandbox* game dat door Mojang is uitgebracht in 2011. (Zit standaard in Windows)  

**Blender**: Is een opensourceprogramma voor het maken van 3D-computergraphics en computeranimaties. (Zit niet standaard in Windows)  

**Gnome Maps**: Is een gratis map applicatie waarmee je simpele mappen kunt bekijken. (Zit niet standaard in Windows)  

**Sublime:** Is een cross-platform-editor voor tekst en broncode. (Zit niet standaard in Windows)  

**VLC**: Is een vrije en opensource mediaspeler, die deel uitmaakt van het VideoLAN project. (Zit niet standaard in Windows)  

**Steam**: Is een software-distributieplatform ontwikkeld door Valve Corporation, hoofdzakelijk bedoeld voor computerspellen. (Zit standaard in Windows)  

**Gparted**: Is een programma om partities op de harde schijf te creëren, verwijderen, formatteren, verplaatsen, verkleinen en vergroten, controleren en kopiëren, met als doel het reorganiseren van een harde schijf. (Zit niet standaard in Windows)  

**Whatsapp**: Is een waarmee je snel en eenvoudig en veilig gratis berichten versturen en bellen op telefoons over de hele wereld. (Zit standaard in Windows

**Spotify** is een dienst van het Zweedse bedrijf Spotify Technology S.A. Er wordt muziek aangeboden in de vorm van *streaming media* via het internet. (Zit standaard in Windows)