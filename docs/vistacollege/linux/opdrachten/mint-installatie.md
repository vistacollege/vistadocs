

# Opdracht 3 - Installatie

## USB/Schijf opstarten
Zodra je de machine opstart met de de USB stick (Of virtuele omgeving), zal hij banzelf opstarten in het installatie programma, zoniet. En je komt in dit menu:  
![cinimon-choice-menu](https://i.imgur.com/XgVwpfS.png)  
Als dit gebeurd, druk dan gewoon op enter als je op ``Start Linux Mint`` staat.

Van hieruit start mint vanzelf op: 
![Image](/vistadocs/images/KHppjMCice.gif)

Uiteindelijk kom je dan op dit scherm: 
![Image](https://i.imgur.com/NLFpF18.png)

## Start de installatie
Als je op dit scherm zit, staat er een CD Disk op het scherm genaamd "Install Linux Mint". 
![Image](https://i.imgur.com/1Q3zWA4.png)

### Taal
Dubbelklik hierop, daarna vraagt mint om de taal die je wilt gebruiken: 
![Image](https://i.imgur.com/Ng9jsg3.png)

### Toetsenbord Layout
Toetsenbord layout (Beste is deze standaard laten): 
![Image](https://i.imgur.com/VAAnWxe.png)

### Extra (grafische) media
Optioneel, niet nodig voor de installatie. Mag wel. 
![Image](https://i.imgur.com/4liXnKY.png)

### Schijvenbeheer
Mint vraagt nu wat je wilt doen met de schijf die gekoppelt zit aan de machine. Dit scherm verschilt per installatie, maar wij laten de instellingen voor nu op de standaard dingen staan. 
![Image](https://i.imgur.com/gE85s5t.png)

Daarna druk je op "Install now": 
![Image](https://i.imgur.com/0MlJaLm.png)

### Tijdzone
Mint vraagt nu om je tijdzone. 
![Image](https://i.imgur.com/A1SpXv0.png)

### Inlog en computergegevens
Als laaste krijg je gevraagd om informatie in te vullen over jouw naam, de naam voor de machine. Wat voor gebruikersnaam je wilt hebben, wat voor wachtwoord en of je elke keer bij het inloggen je wachtwoord moet invoeren, of dat je automatisch inlogt. 
![Image](https://i.imgur.com/z4fEFab.png)

Nadat mint klaar is met het installeren, kan je herstarten (Vergeet niet om de disk/usb uit de machine te halen): 
![Image](https://i.imgur.com/TQxJ8Mm.png)
